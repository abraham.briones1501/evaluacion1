const carpetas = 'datos/';
const fs = require('fs'); 
var archivos = [];


// const readline = require('readline');
// var lector = readline.createInterface({
// 	input: process.stdin,
// 	output: process.stdout
// });
function promedioFuncion(lector) {

    const leerOpcion = (lectura)=>{
        return new Promise((resolve,reject)=>{
            //console.log('Que desea promediar');
            lectura.question('Indicador a promediar: (1. Dolar 2.Euro 3.Tasa de desempleo) ', opcion =>{
                resolve(opcion);
            });
        });
    }

    const leerArchivos =()=>{
        archivos=[];
        return new Promise((resolve,reject)=>{
            fs.readdirSync(carpetas).forEach(file => {
                let fileName=`datos/${file}`;
                let content=fs.readFileSync(fileName,"utf8");
                content=JSON.parse(content);
                archivos.push(content[0]);
                //console.log(file);
                //console.log(content);
            });
        })
    }

    const calcularPromedio=(opcion)=>{
        let sumaDolar=0,sumaEuro=0,sumaTasa=0;
        let promDolar=0,promEuro=0,promTasa=0;
        let fechaInicial=0, fechaFinal=0;
        
        for (let i = 0; i < archivos.length; i++) {
            sumaDolar = sumaDolar + Number(archivos[i].dolar.valor);
            sumaEuro = sumaEuro + Number(archivos[i].euro.valor);
            sumaTasa = sumaTasa + Number(archivos[i].tasa_desempleo.valor);
        }
        
        promDolar=sumaDolar/archivos.length;
        promEuro=sumaEuro/archivos.length;
        promTasa=sumaTasa/archivos.length;
        
        fechaInicial = (archivos[0].fecha);
        fechaFinal = (archivos[archivos.length-1].fecha);
        
        if(opcion=='1'){
            console.log('Desde el: '+fechaInicial+' Hasta el: '+fechaFinal);
            console.log('Promedio del dolar: '+promDolar);
        }
        if(opcion=='2'){
            console.log('Desde el: '+fechaInicial+' Hasta el: '+fechaFinal);
            console.log('Promedio del euro: '+promEuro);
        }
        if(opcion=='3'){
            console.log('Desde el: '+fechaInicial+' Hasta el: '+fechaFinal);
            console.log('Promedio de la tasa de desempleo: '+promTasa);
        }

        lector.close();
    }


   // leerOpcion(lector).then(  (opcion)=>{ (calcularPromedio(opcion)); } ).catch(console.error)

    //leerArchivos().then(calcularPromedio()).catch(console.error);
    leerArchivos().then(leerOpcion(lector).then( (opcion)=>{ (calcularPromedio(opcion))}).catch(console.error));
}
module.exports = promedioFuncion;