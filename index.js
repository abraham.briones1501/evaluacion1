
const readline = require('readline');
var actualizarFuncion = require('./librerias/dataAPI.js');

var promedioFuncion = require('./librerias/promedio.js');

var lector = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

function actualizarFuncionCallBack(){
	actualizarFuncion();
	menu();
}



function menu(){
    console.log('Menu');
    console.log('1.- Actualizar indicadores');
    console.log('2.- Promediar');
    console.log('3.- Mostrar valor actual');
    console.log('4.- Mostrar mínimo histórico');    
    console.log('5.- Mostrar máximo histórico');
    console.log('6.- Salir');
    lector.question('Escriba su opcion: ', opcion =>{
        switch(opcion){
            case '1': console.log('Actualizar indicadores');
                actualizarFuncionCallBack();
                menu();
            break;
            case '2': //console.log('Promedio de indicadores');
                promedioFuncion(lector);
                menu();
            break;
            case '6': console.log('Opcion 6');
                console.log('Adios!');
                lector.close();
                process.exit(0);
            break;
            default: 
            console.log('Opcion no encontrada');
            break;
        }
    });
}

menu();
